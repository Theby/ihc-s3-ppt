# Objetivos del sprint
## Objetivos del sprint

 * Crear todas las funcionalidades de TodoAgil++ para los alumnos y el administrador.
 * Guardianes de estándares.
 * Ayudar con la integración y navegación de la aplicación.





# Roles
## Roles

 * Felipe Garay: Programador, tester.
 * Patricio Jara: Programador, QA, tester.
 * Diego Diaz: Diseñador, tester.
 * Kevin Farias: Documentador.
 * Luis Celedon: Documentador.
 * Esteban Gaete: Scrum Master, apoyo en otros roles.






# Retrospectiva
## ¿Qué nos gustaría empezar a hacer?

 * Tener jornadas de trabajo en grupo
 * Realizar TDD.

## ¿Qué nos gustaría seguir haciendo?

 * Utilizar hipchat como medio de comunicación
 * Usar netbeans para generar prototipos de test unitarios

## ¿Qué nos gustaría dejar de hacer?

 * Trabajar de noche
 * Despreocuparnos del proyecto debido a otras prioridades





# Planificación
## Carta Gantt

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{gantt.PNG}
\caption{Carta Gantt}
\end{figure}

## Organización del sprint

 * Durante este sprint se siguió trabajando con git-flow. 
 * También se implementó una modalidad en la que si un grupo hacía un pull request
   al repositorio base, este debe contar con un approve y un comentario de alguien
   del grupo 1 para poder ser implementado en la base.
 * Generación de Reportes.
 * Implementación de nueva minuta para reuniones diarias.

## Rituales

 * Sprint Planning Meeting
 * Daily Meetings
 * Demo Review Meeting
 * Review Meeting
 * Sprint Retrospective Meeting

## Grafico de asistencias

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{daily.png}
\end{figure}

# Artefactos
## Sprint Backlog

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{sprintBacklog.png}
\caption{Sprint Backlog del Sprint 3}
\end{figure}

## Otros artefactos

 * Minutas de Daily Meetings
 * Minutas de Reportes con el Jefe del proyecto
 * Carta Gantt
 * Vistas en Balsamiq







# Métricas
## Nielsen y Gestalt

En la aplicación se han realizado análisis sobre la usabilidad en cada una de 
las diferentes vistas. Estos análisis han sido realizados usando las tablas
presentadas en clases.


## Tabla de puntaje

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{tablaPuntaje.png}
\caption{Tabla de Puntajes de Nielsen}
\end{figure}

## Tabla de usabilidad

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{tablaUsabilidad.png}
\caption{Tabla de Usabilidad de Nielsen}
\end{figure}

## Tabla de Gestalt

\begin{figure}[t]
\centering
\includegraphics[scale=0.45]{tablaGestalt.png}
\caption{Tabla de Gestalt}
\end{figure}


## Burndown chart

\begin{figure}[t]
\centering
\includegraphics[scale=0.3]{burndown.png}
\caption{Burndown Sprint 3}
\end{figure}





# Reportes
## Reportes

Se realizaron tres reportes durante este Sprint:

 * Cada reporte consta de la participación de cada miembro, su complejidad aportada,
tareas e historias abordadas, así como también problemas con otros grupos.
 * Al comparar los tres reportes podemos concluir que dos de los principales problemas se deben
principalmente a la ausencia de dos miembros durante una semana debido a la JCC en Talca y al
retraso de una tarea que se logró terminar antes del fin del sprint.






# Problemas
## Problemas del grupo

En particular estas últimas semanas han sido complicadas para la disponibilidad de trabajo,
contamos con la semana informática, la jornada chilena de la computación y entregas de laboratorios
en diversos ramos, como lo son Proyecto de Ingeniería de Software y Redes Neuronales.

Esto ha producido que el tiempo dedicado al proyecto de IHC no haya sido el necesario para algunos
miembros del equipo.

## Alcances con otros grupos

Durante la etapa de vistas de balsamiq hubo un problema de comunicación con el grupo 3
lo que hizo que la solicitud de vistas haya tomado un poco más de tiempo.

También con el grupo 3 se intentó llegar a un nuevo acuerdo sobre colores para la aplicación,
pero debido a problemas de tiempo esto no se ha llevado a cabo en su totalidad.






# Estándares
## Estándares

El principal estándar que se cuido fue el de seguir el estándar de Google para Java,
en conjunto con Javadocs.

También se iba a insistir en los estándares visuales, pero debido a que el estándar anterior
retrasaba enormemente los pull request, estos últimos estándar no se regularizaron firmemente
para poder facilitar la integración de la aplicación.








# Conclusiones
## Conclusiones

Durante el desarrollo de este Sprint nos hemos dado cuenta que para sacar adelante un software 
es necesario que todo el equipo trabaje, y que trabaje como equipo. Esto fue difícil de lograr
debido a la ausencia de algunos miembros durante algunos días, así como también la natural preferencia
por trabajar en otros ramos antes de IHC. 

Estos factores mermaron el avance del proyecto, por lo que no se pudo poner mayor énfasis a algunos puntos
que podrían haber aumentado la calidad del software, como por ejemplo haber aplicado las fallas de usabilidad
encontradas.